#!/usr/bin/env python3
#
# Copyright 2019 Apex.AI, Inc.
#
# SPDX-License-Identifier: Apache-2.0
"""
This script parses CHANGES.rst to get the section for a given release.
"""

import argparse


def main():
    parser = argparse.ArgumentParser(description='Parse Changelog and extract'
                                                 'current release information')
    parser.add_argument('--release-version', type=str,
                        help='The release version, e.g., v1.0.0rc5',
                        required=True)
    parser.add_argument('--change-log', type=argparse.FileType('r'),
                        required=True, help='Path to change log')

    args = parser.parse_args()

    started = False
    lines = []
    for line in args.change_log:
        line = line.strip()
        if not started and line.startswith(f'.. _{args.release_version}:'):
            started = True
        elif started and line.startswith('.. _v'):
            started = False
            break
        elif started:
            lines.append(line)

    return '\n'.join(lines).strip()


if __name__ == "__main__":
    print(main())
